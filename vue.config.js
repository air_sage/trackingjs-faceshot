module.exports = {
    lintOnSave: false,
    // baseUrl: './',
    publicPath: process.env.NODE_ENV === 'production' ? './' : '/',
    // assetsPublicPath: process.env.NODE_ENV === 'production' ? './' : '/',
    outputDir: 'dist',
    configureWebpack: {
        externals: {

        }
    }
}